package com.minderaschool.banks;

public class Account {
    private int id;
    private String name;
    private double balance;


    //Constructor
    public Account(int id ,String name, double balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    //SET ID

    public void setId(int id) {
        this.id = id;
    }

    //Get ID
    public int getId() {
        return id;
    }

    //Get Balance
    public double getBalance() {
        return balance;
    }

    //Get Name
    public String getName() {
        return name;
    }


    //Withdraw
    public boolean withdraw(double amount){
        if(this.balance>=amount){
            this.balance-=amount;
            return true;
        }
        return false;
    }

    //Deposit
    public void deposit(double amount){
        this.balance+=amount;
    }


    //Transfer
    public boolean transfer(double amount,Account dest){
        if(withdraw(amount)){
            dest.deposit(amount);
            return true;
        }
        return false;
    }

}