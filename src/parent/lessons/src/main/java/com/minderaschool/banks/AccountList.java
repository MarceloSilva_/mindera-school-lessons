package com.minderaschool.banks;


public class AccountList {
    private Account[] accounts = new Account[10];
    private int n;

    public void add(Account acc){
        this.n++;
        if(n == accounts.length){
            Account[] temp = accounts;
            accounts = new Account[accounts.length*2];
            for (int i=0; i<temp.length; i++){
                accounts[i] = temp[i];
            }
        }
        accounts[n] = acc;
    }

    public Account get(int i){
        return accounts[i];
        //return "[ Name: '" + accounts[i].getName() + "' , Balance: " + accounts[i].getBalance() + " ]";
    }

    public String getAllData(){
        String result= "";
        for (int i=0; i<accounts.length ; i++){
            if(accounts[i]!=null)
                result += "\n[ ID: "+ accounts[i].getId() +" , Name: '" + accounts[i].getName() + "' , Balance: " + accounts[i].getBalance() + " ]";
        }
        return result;
        //return "[ Name: '" + accounts[i].getName() + "' , Balance: " + accounts[i].getBalance() + " ]";
    }

    public Account[] getAll(){
        Account[] all = new Account[n];
        for(int i=0; i<n && accounts[i] !=null;i++){
            all[i] = accounts[i];
        }
        return all;
    }

    public void remove(int index){
        for(int i=index;i<n;i++){
            accounts[i] = accounts[i+1];
            accounts[i].setId(i);
        }
        accounts[n] = null;
        this.n--;
    }
    public  int count(){
        return n;
    }

}
