package com.minderaschool.banks;

import com.minderaschool.banks.Account;
import com.minderaschool.banks.AccountList;

import java.util.Scanner;

public class Application {
    public static AccountList accountList = new AccountList();

    public static void main(String[] args){

        accountList.add(new Account(0,"a",1));
        accountList.add(new Account(1,"b",2));
        accountList.add(new Account(2,"c",3));

        AccountList list1 = new AccountList();
        list1.add(new Account(0,"a",1));
        list1.add(new Account(1,"b",2));
        System.out.println(list1.count()); // output: 2

        AccountList list2 = new AccountList();
        System.out.println(list2.count()); // output: 2
        System.out.println(list1.getAll());

        menu();
    }

    public static void selectAccount(){
        int id;
        Scanner sc = new Scanner(System.in);
        System.out.println("------ ACCOUNT ID ------");
        System.out.println("Account id: ");
        id = sc.nextInt();
        accountMenu(id);
    }

    public static void accountMenu(int id){
        Scanner sc = new Scanner(System.in);
        int opc;
        int amount;
        int tranferId;
        Account account = accountList.get(id);
        do {
            System.out.println("------ ACCOUNT OPTION ------");
            System.out.println("ID: " + account.getId() + " , Name: " + account.getName() + " , Balance: " + account.getBalance());
            System.out.println("1 - Withdraw");
            System.out.println("2 - Deposit");
            System.out.println("3 - Transfer");
            System.out.println("4 - Delete account");
            System.out.println("0 - Exit");
            System.out.print("Option: ");
            opc = sc.nextInt();
            switch (opc){
                case 1:
                    System.out.println("Amount: ");
                    amount = sc.nextInt();
                    if(account.withdraw(amount))
                        System.out.println("Successful withdraw");
                    else{
                        System.out.println("Error!!");
                    }
                    break;
                case 2:
                    System.out.println("Amount: ");
                    amount = sc.nextInt();
                    account.deposit(amount);
                    break;
                case 3:
                    System.out.println("Amount: ");
                    amount = sc.nextInt();
                    System.out.println("Account Id: ");
                    tranferId = sc.nextInt();
                    if(account.transfer(amount, accountList.get(tranferId)))
                        System.out.println("Successful transfer");
                    else{
                        System.out.println("Error!!");
                    }
                    break;
                case 4:
                    accountList.remove(id);
                    opc=0;
                    break;
            }
        }while (opc!=0);
    }

    public static void listAll(){
        System.out.println(accountList.getAllData());
        System.out.println("Total Accounts: "+ accountList.count());
    }

    public static void addAccount(){
        Scanner sc = new Scanner(System.in);
        String name;
        int balance;
        System.out.println("------ NEW ACCOUNT ------");
        System.out.print("Name: ");
        name= sc.nextLine();
        System.out.print("Balance: ");
        balance= sc.nextInt();
        Account acc = new Account(accountList.count(),name,balance);
        accountList.add(acc);
    }

    public static void menu(){
        int opc;
        do{
            Scanner sc = new Scanner(System.in);
            System.out.println("----------------------");
            System.out.println("1 - Add Account");
            System.out.println("2 - List Accounts");
            System.out.println("3 - Select Account");
            System.out.println("0 - Exit");
            System.out.println("----------------------");
            System.out.print("Option: ");
            opc =sc.nextInt();
            switch (opc){
                case 1:
                    addAccount();
                    break;
                case 2:
                    listAll();
                    break;
                case 3:
                    selectAccount();
                    break;
            }
        }while (opc!=0);
    }
}
