package com.minderaschool.cars;

public class Body {

    private Door[] doors;

    public Body(int numDoor) {
        doors = new Door[numDoor];
        for(int i=0; i<numDoor; i++){
            doors[i] = new Door();
        }
    }

    public void openDoor(int num){
        this.doors[num].open();
    }

    public void closeDoor(int num){
        this.doors[num].close();
    }
    public void isOpen(int num){
        this.doors[num].isOpen();
    }
}
