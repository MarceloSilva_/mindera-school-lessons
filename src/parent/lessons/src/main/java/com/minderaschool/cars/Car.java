package com.minderaschool.cars;

import com.minderaschool.utils.Logger;

public class Car {
    Logger logger = new Logger("Car");
    private Engine engine;
    private Tire[] tires;
    private Body body;

    public Car(int horsePower, int numTires, int numDoors) {
        engine = new Engine(horsePower);
        tires = new Tire[numTires];
        for(int i=0; i<numTires; i++){
            tires[i] = new Tire();
        }
        body = new Body(numDoors);
    }

    public Car(int numTires, int numDoors) {
        engine = new Engine();
        tires = new Tire[numTires];
        for(int i=0; i<numTires; i++){
            tires[i] = new Tire();
        }
        body = new Body(numDoors);
    }

    public void move() {
        if(!engine.isStarted())
            engine.start();
        for (Tire tire : tires)
            tire.rotate();
    }

    public void openDoor(int num) {
        body.openDoor(num);
    }
    public void closeDoor(int num) {
        body.closeDoor(num);
    }
}
