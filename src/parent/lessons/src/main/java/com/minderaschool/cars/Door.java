package com.minderaschool.cars;

import com.minderaschool.utils.Logger;

public class Door {
    private static final Logger LOGGER = new Logger("Door");
    private boolean open;

    public boolean isOpen() {
        return open;
    }

    public void open(){
        LOGGER.info("Door Open");
        this.open = true;
    }

    public void close(){
        LOGGER.info("Door Closed");
        this.open = false;
    }
}
