package com.minderaschool.cars;

import com.minderaschool.utils.Logger;

public class Engine {
    private int horsePower;
    private boolean started;

    //Constructor
    public Engine() {

    }
    public Engine(int horsePower) {
        this.horsePower = horsePower;
    }

    // Getters && Setters
    public boolean isStarted() {
        return started;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void start(){
        this.started = true;
    }

    public void stop(){
        this.started = false;
    }
}
