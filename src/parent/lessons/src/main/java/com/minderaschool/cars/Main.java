package com.minderaschool.cars;

public class Main {

    public static void main(String[] args){
        Car car = new Car(4,5);
        Car car1 = new Car(1200,4,5);

        car1.openDoor(1);
        car1.closeDoor(1);

        car1.move();
        car1.move();
    }
}
