package com.minderaschool.cars;

import com.minderaschool.utils.Logger;

public class Tire {
    private static final Logger LOGGER = new Logger("Tire");

    public void rotate() {
        LOGGER.info("Rotating");
    }
}
