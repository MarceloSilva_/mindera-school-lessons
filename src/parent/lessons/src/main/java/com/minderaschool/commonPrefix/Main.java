package com.minderaschool.commonPrefix;

public class Main {
    public static void main(String[] args) {
        String[] input = new String[]{"aaa", "aaaab", "aaaab", "aaaa", "aaaa"};
        String output = getCommonPrefix(input);
        System.out.print("Input: ");
        for (String word : input) {
            System.out.print(word + ",");
        }
        System.out.println("\nOutput: " + output);
    }



    public static String getCommonPrefix(String[] words) {
        if (words.length == 0) {
            return "";
        }

        String result = ""; //

        int count = 0;
        int i = 0;


        while (isCountValid(words, i, count) && charsAreEqual(count, words[i], words[i + 1])) {
            i++;
            if (i >= words.length - 1) {
                i = 0;
                result += words[i].charAt(count);
                count++;
            }
        }

        return result;
    }

    private static boolean charsAreEqual(int count, String atual, String next) {
        return atual.charAt(count) == next.charAt(count);
    }

    private static boolean isCountValid(String[] words, int i, int count) {
        boolean emptyWords = words[i].isEmpty() && words[i + 1].isEmpty();
        boolean smallerThanCount = words[i].length() > count && words[i + 1].length() > count;
        return !emptyWords && smallerThanCount;
    }
}
