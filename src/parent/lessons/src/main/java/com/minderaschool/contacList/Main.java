package com.minderaschool.contacList;

import java.util.*;

public class Main {

    public static List<Person> persons = new ArrayList<Person>() {{
        add(new Person("Marco", new Contact("Email", "marco.escaleira@a.com")));
        add(new Person("Marcelo", new Contact("Email", "marcelo.silva@a.com")));
        add(new Person("Pedro", new Contact("Email", "pedro.silva@a.com")));
        add(new Person("Marco", new Contact("phone", "917883315")));
        add(new Person("Marco", new Contact("phone", "917883316")));
        add(new Person("Pedro", new Contact("phone", "788335756")));
    }};

    public static void main(String[] args) {
        print(persons);
        List<Person> teste = merge(persons);
        System.out.println("++++++++++++++");
        print(teste);
    }


    public static List<Person> merge(List<Person> persons) {
        Map<String, Person> map = new HashMap<>();

        for (Person person : persons) {
            String name = person.getName();
            if (map.containsKey(name)) {

                Person p = map.get(name);
                List<Contact> contacts = new ArrayList<>();
                contacts.addAll(p.getContacts());

                for (Contact pCon : person.getContacts()) {//              >>>>>>>>>  contacts.addAll(person.getContacts());
                    boolean adicionar=true;
                    for (Contact con : contacts) {
                        if (pCon.getType().equals(con.getType()) && pCon.getValue().equals(con.getValue())) {
                            adicionar = false;
                            break;
                        }
                    }
                    if(adicionar){
                        contacts.add(pCon);
                    }
                }


                map.replace(name, new Person(name, contacts));
            } else {
                map.put(name, person);
            }
        }

        return new ArrayList<>(map.values());
    }

    public static void print(List<Person> per) {
        for (Person person : per) {
            System.out.print(person.getName() + "\n");
            for (Contact contact : person.getContacts()) {
                System.out.println(" " + contact.getType() + ":" + contact.getValue());
            }
            System.out.println();
        }
    }
}
