package com.minderaschool.exams.Paper16_11_2018;

public class Comment {
    private String text;
    private String username;

    public Comment(String text, String username) {
        this.text = text;
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public String getUsername() {
        return username;
    }
}
