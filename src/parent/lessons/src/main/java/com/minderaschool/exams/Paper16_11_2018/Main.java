package com.minderaschool.exams.Paper16_11_2018;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Post p1 = new Post("yay!! I am Happy! ", "kenny_mccormick");
        p1.like("eric_cartman");
        p1.like("kyle_browlovsky");
        p1.comment("stan_marsh", "Kenny!!!");

        Post p2 = new Post("Guys! I'm going home!", "eric_cartman");
        p2.like("kyle_browlovsky");

        Post p3 = new Post("BearManPig is real!!!", "stan_marsh");
        p3.comment("I warned you!", "al_gore");
        p3.comment(":0", "kyle_browlovsky");

        List<Post> posts = new ArrayList<>() {{
            add(p1);
            add(p2);
            add(p3);
        }};


        printTimeline(posts);
    }

    public static void printTimeline(List<Post> posts) {
        for (Post post : posts) {
            post.print();
        }
    }
}
