package com.minderaschool.exams.Paper16_11_2018;

import java.util.ArrayList;
import java.util.List;

public class Post {

    public String text;
    public String username;
    private List<String> likes;
    private List<Comment> comments;

    public Post(String text, String username) {
        this.text = text;
        this.username = username;
        this.likes = new ArrayList<>();
        this.comments = new ArrayList<>();
    }

    public void like(String username) {
        for (int i = 0; i < likes.size(); i++) {
            if (likes.get(i).equals(username)) {
                return;
            }
        }
        likes.add(username);
    }

    public void unlike(String username) {
        for (int i = 0; i < likes.size(); i++) {
            if (likes.get(i).equals(username)) {
                likes.remove(i);
                return;
            }
        }
    }

    public String[] getLikes() {
        String[] output = new String[likes.size()];

        for (int i = 0; i < likes.size(); i++) {
            output[i] = likes.get(i);

        }
        return output;
    }

    public void comment(String username, String text) {
        comments.add(new Comment(text, username));
    }

    public Comment[] getComments() {
        Comment[] output = new Comment[comments.size()];
        for (int i = 0; i < comments.size(); i++) {
            output[i] = comments.get(i);
        }
        return output;
    }

    public void print() {
        System.out.print("---\n" + '"' + text + '"' + " - " + username + "\n---\nLikes: ");
        if (likes.size() > 0) {
            for (int i = 0; i < likes.size(); i++) {
                if (i == likes.size() - 1) {
                    System.out.print(likes.get(i));
                } else {
                    System.out.print(likes.get(i) + ", ");
                }
            }
        } else {
            System.out.print("No Likes");

        }
        System.out.println("\n---");
        if (comments.size() > 0) {
            for (int i = 0; i < comments.size(); i++) {
                System.out.println("Comment: " + comments.get(i).getText() + " - " + comments.get(i).getText());
            }
        } else {
            System.out.println("No Comments");

        }
        System.out.println("---");
    }
}
