package com.minderaschool.fibonacciTreads;

public class Fibonacci {

    public Long fibonacci(Long n) throws InterruptedException {
        if (!Memoization.containsKey(n)) {
            Thread t = new Thread(new MyRunnable(n));
            t.start();
            t.join();
        }
        return Memoization.get(n);
    }


    public class MyRunnable implements Runnable {

        private Long n;

        public MyRunnable(Long n) {
            this.n = n;
        }

        @Override
        public void run() {
            if (n < 0) {
                throw new IllegalArgumentException();
            }
            if (n <= 1) {
                Memoization.add(n, 1L);
            } else if (!Memoization.containsKey(n)) {
                try {
                    //contains both n-1 && n-2
                    if (Memoization.containsKey(n - 1) && Memoization.containsKey(n - 2)) {
                        Memoization.add(n, (Memoization.get(n - 1) + Memoization.get(n - 2)));
                        //contain  only n-2
                    } else if (!Memoization.containsKey(n - 1) && Memoization.containsKey(n - 2)) {
                        Memoization.add(n, (fibonacci(n - 1) + Memoization.get(n - 2)));
                        //contain  only n-1
                    } else if (Memoization.containsKey(n - 1) && !Memoization.containsKey(n - 2)) {
                        Memoization.add(n, (Memoization.get(n - 1) + fibonacci(n - 2)));
                    } else {
                        Memoization.add(n, (fibonacci(n - 1) + fibonacci(n - 2)));
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
