package com.minderaschool.heap;

public class Heap {
    public static void main(String[] args) {
        System.out.println("Min: " + minHeap(new int[]{0,3,4,5,10,7,15}));
        System.out.println("Min: " + minHeap(new int[]{0,10,11,15,1,12,13}));

        System.out.println("Max: " + maxHeap(new int[]{10, 9, 5, 6, 3, 2}));
    }

    public static boolean minHeap(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            if(arr[(i-1)/2] > arr[i]){
                return false;
            }
        }
        return true;
    }

    public static boolean maxHeap(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            if(arr[(i-1)/2] < arr[i]){
                return false;
            }
        }
        return true;
    }
}
