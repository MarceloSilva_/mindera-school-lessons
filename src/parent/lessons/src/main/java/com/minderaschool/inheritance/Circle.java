package com.minderaschool.inheritance;

public class Circle extends Figure {

    public Circle(double radius){
        super.area = Math.PI*(radius*radius);
        super.IAm = "Circle";
    }

    public double area(){
        return area;
    }

    public void whoAmI(){
        System.out.println(IAm);
    }
}
