package com.minderaschool.inheritance;

public class Figure {

    String IAm = "Figure";
    double area = 0.0;

    public double area(){
        return area;
    }

    public void whoAmI(){
        System.out.println(IAm);
    }

}
