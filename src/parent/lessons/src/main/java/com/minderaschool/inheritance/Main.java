package com.minderaschool.inheritance;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){

        ArrayList<Figure> lista = new ArrayList<>();
        lista.add(new Rectangle(10,5));
        lista.add(new Square(11));
        lista.add(new Circle(5));
        lista.add(new Triangle(23,6));

        for (Figure fig : lista){
            fig.whoAmI();
            System.out.println("Area: " + fig.area);
            System.out.println();
        }
    }

}
