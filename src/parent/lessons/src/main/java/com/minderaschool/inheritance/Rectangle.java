package com.minderaschool.inheritance;

public class Rectangle extends Figure {

    public Rectangle(){

    }

    public Rectangle(double length, double width){
        super.area = length*width;
        super.IAm = "Rectangle";
    }

    public double area(){
        return area;
    }

    public void whoAmI(){
        System.out.println(IAm);
    }
}

