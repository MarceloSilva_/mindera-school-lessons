package com.minderaschool.inheritance;

public class Square extends Rectangle {


    public Square(double side){
        super.area = side*side;
        super.IAm = "Square";
    }

    public double area(){
        return area;
    }

    public void whoAmI(){
        System.out.println(IAm);
    }
}
