package com.minderaschool.inheritance;

public class Triangle extends Figure {

    public Triangle(double base, double height) {
        super.area = (base * height) / 2;
        super.IAm = "Triangle";
    }

    public double area() {
        return area;
    }

    public void whoAmI() {
        System.out.println(IAm);
    }
}
