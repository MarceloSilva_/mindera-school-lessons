package com.minderaschool.linkedList;

public class LinkedList {

    private Node head;
    private Node tail;
    private int size = 0;

    public void addAtHead(int val) {
        Node nodeToBeInserted = new Node(val);
        if (head == null) {
            head = nodeToBeInserted;
            tail = head;
        } else {
            nodeToBeInserted.nextNode = head;
            head = nodeToBeInserted;
        }
        size++;
    }

    public void addAtTail(int val) {
        Node nodeToBeInserted = new Node(val);
        tail.nextNode = nodeToBeInserted;
        size++;
    }

    public void addAtIndex(int index, int val) {
        if (index == 0) {
            addAtHead(val);
            return;
        }
        if (index == size) {
            addAtTail(val);
            return;
        }

        Node nodeToBeInserted = new Node(val);
        Node temporaryNode = head;
        for (int i = 1; i < index; i++) {
            temporaryNode = temporaryNode.nextNode;
        }
        nodeToBeInserted.nextNode = temporaryNode.nextNode;
        temporaryNode.nextNode = nodeToBeInserted;
        size++;
    }

    private Node getNode(int index) {
        validateIndex(index);

        Node temporaryNode = head;

        int i = 0;

        while (temporaryNode != null && i < index) {
            temporaryNode = temporaryNode.nextNode;
            i++;
        }

        return temporaryNode;
    }

    private void validateIndex(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
    }

    public int get(int index) {
        return getNode(index).value;
    }

    public void deleteAtIndex(int index) {
        validateIndex(index);

        if (index == 0) {
            head = head.nextNode;
            return;
        }

        Node parent = getNode(index - 1);
        parent.nextNode = parent.nextNode.nextNode;

        size--;
    }

    public boolean isPalindrome() {
        LinkedList reversedList = reverse();
        return equals(reversedList);
    }

    private boolean equals(LinkedList list) {
        if (list.size != this.size) {
            return false;
        }

        Node current = head;
        Node otherCurrent = list.head;

        do {
            if (current.value != otherCurrent.value) {
                return false;
            }
            current = current.nextNode;
            otherCurrent = otherCurrent.nextNode;
        } while (current.nextNode != null);
        return true;
    }

    private LinkedList reverse() {
        LinkedList reversedList = new LinkedList();
        reversedList.addAtHead(head.value);
        Node ptr = head;
        while (ptr.nextNode != null) {
            ptr = ptr.nextNode;
            reversedList.addAtHead(ptr.value);
        }

        return reversedList;
    }

    private static class Node {
        private int value;
        private Node nextNode;

        Node(int val) {
            this.value = val;
        }
    }

    public String toString() {
        Node current = head;
        String s = "[";
        for (int i = 0; i < size; i++) {
            s += current.value;
            current = current.nextNode;
            if (current != null) {
                s += ",";
            }
        }
        return s + "]";
    }

    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.addAtHead(1); System.out.println(list.toString());
        list.addAtTail(3); System.out.println(list.toString());
        list.addAtIndex(1, 2); System.out.println(list.toString());
        list.addAtIndex(1, 5); System.out.println(list.toString());
        System.out.println(list.get(1)); System.out.println(list.toString());
        list.deleteAtIndex(1); System.out.println(list.toString());
        System.out.println(list.get(1)); System.out.println(list.toString());
    }
}

