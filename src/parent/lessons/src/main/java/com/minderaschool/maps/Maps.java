package com.minderaschool.maps;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


public class Maps {
    public static void main(String[] args) {
        //ex1();
        //ex2();
        ex3("Hello World");
    }

    public static Integer[] getInteger(Integer[] array, int num) {
        Integer[] newArray = new Integer[array.length + 1];
        int i;
        for (i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }
        newArray[i] = num;
        return newArray;
    }

    public static void printOrder(HashMap<Character, Integer[]> map, char[] word) {
        boolean[] found = new boolean[256];
        StringBuilder sb = new StringBuilder();
        for (char c : word) {
            if (!found[c]) {
                found[c] = true;
                sb.append(c);
            }
        }
        word = sb.toString().toCharArray();
        for (char letter : word) {
            for (Map.Entry entry : map.entrySet()) {
                if (entry.getKey().equals(letter)) {
                    System.out.print(entry.getKey() + "=[");
                    Integer[] v = (Integer[]) entry.getValue();
                    for (int i = 0; i < v.length; i++) {
                        System.out.print(v[i]);
                        if (i != v.length - 1) {
                            System.out.print(",");
                        }
                    }
                    System.out.print("] ");

                }
            }
        }
    }

    public static void ex3(String string) {
        char[] word = string.replaceAll(" ", "").toCharArray();
        HashMap<Character, Integer[]> map = new HashMap<>();
        for (int i = 0; i < word.length; i++) {
            char letter = word[i];
            //System.out.println();
            //System.out.println(letter);
            if (map.containsKey(letter)) {
                //System.out.println("ENCONTROU");
                for (Map.Entry entry : map.entrySet()) {
                    if (entry.getKey().equals(letter)) {
                        Integer[] array = getInteger((Integer[]) entry.getValue(), i);
                        entry.setValue(array);
                        //System.out.println("KEY: " + entry.getKey());
                        break;
                    }
                }
            } else {
                //System.out.println("NOVO");
                map.put(word[i], new Integer[]{i});
            }
        }

        map.forEach((k, v) -> {
            System.out.print(k + "=[");
            for (int i = 0; i < v.length; i++) {
                System.out.print(v[i]);
                if (i != v.length - 1) {
                    System.out.print(",");
                }
            }
            System.out.print("] ");
        });
        System.out.println();
        printOrder(map, word);
    }


    public static void ex2() {
        HashMap<Integer, Character> map = new HashMap<>() {{
            put(-2, 'b');
            put(-3, 'c');
            put(-4, 'd');
            put(-5, 'a');
            put(-6, 'a');
            put(-7, 'b');
            put(-8, 'c');
            put(-9, 'a');
        }};

        HashMap<Integer, Character> print = new HashMap<>();

        for (Map.Entry<Integer, Character> entry : map.entrySet()) {
            Integer key = null;
            if (print.containsValue(entry.getValue())) {
                for (Map.Entry entry1 : print.entrySet()) {
                    if (entry1.getValue().equals(entry.getValue())) {
                        key = (Integer) entry1.getKey();
                        //System.out.println("KEY: " + key);
                        break;
                    }
                }
                print.remove(key);

            }
            print.put(entry.getKey(), entry.getValue());

        }
        System.out.println(print);
    }

    public static void ex1() {
        Map<String, Integer> map = new HashMap<>();

        String[] strings = new String[]{"dd", "dd", "cc", "dd", "cc", "dd", "aa", "aa", "aa"};

        for (String string : strings) {
            if (!map.containsKey(string)) {
                map.put(string, 1);
            } else {
                int count = map.get(string) + 1;
                map.replace(string, count);
            }
        }

        //LAMBDA MAX
        final AtomicInteger maxValue = new AtomicInteger();
        final Stack maxKey = new Stack();
        map.forEach((k, v) -> {
            if (v > maxValue.get()) {
                maxKey.push(k);
                maxValue.set(v);
            }
        });
        System.out.println(maxKey.pop() + " " + maxValue);
    }
}
