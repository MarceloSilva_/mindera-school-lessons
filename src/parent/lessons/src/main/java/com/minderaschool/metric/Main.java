package com.minderaschool.metric;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        List<Metric> metrics = readAndParseFile("datadog-metrics.txt");
        //print(metrics);
        Map<String, String> params = new HashMap<>();

        params.put("cloud", "aws");
        params.put("application", "payment-orchestration");
        List<Metric> result = queryMetrics(metrics, "http.server.requests", params);

        print(result);

    }

    //Search by name and exclude the ones without the right lables
    public static List<Metric> queryMetrics(List<Metric> metrics, String name, Map<String, String> lables) {
        List<Metric> result = new ArrayList<>();

        for (Metric metric : metrics) {
            if (metric.getName().equals(name)) {
                int found = 0;
                for (Map.Entry entry : lables.entrySet()) { //Compares Name
                    if (metric.getLabels().containsKey(entry.getKey())) {//Checks if metric contains the labels
                        if (metric.getLabels().get(entry.getKey()).equals(entry.getValue())) {// Compares the value
                            found++;
                        } else {
                            found = 0;
                            break;

                        }
                    } else {
                        found = 0;
                        break;

                    }
                }
                if (found == lables.size()) {//If all labels are correct add to the result list
                    result.add(metric);
                }
            }
        }
        return result;
    }

    //Exclude the ones without the right lables
    public static List<Metric> queryMetrics(List<Metric> metrics, Map<String, String> lables) {
        List<Metric> result = new ArrayList<>();

        for (Metric metric : metrics) {
            int found = 0;
            for (Map.Entry entry : lables.entrySet()) {
                if (metric.getLabels().containsKey(entry.getKey())) { //Checks if metric contains the labels
                    if (metric.getLabels().get(entry.getKey()).equals(entry.getValue())) { // Compares the value
                        found++;
                    } else {
                        found = 0;
                        break;

                    }
                } else {
                    found = 0;
                    break;

                }
            }
            if (found == lables.size()) { //If all labels are correct add to the result list
                result.add(metric);
            }
        }
        return result;
    }

    public static void print(List<Metric> metrics) {
        for (Metric metric : metrics) {
            System.out.println(metric.getName() + " : " + metric.getValue() + " | " + metric.getType());
            for (Map.Entry entry : metric.getLabels().entrySet()) {
                System.out.println("    " + entry.getKey() + " : " + entry.getValue());
            }
        }
    }

    public static List<Metric> readAndParseFile(String file) {
        List<Metric> metrics = new ArrayList<>();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = reader.readLine()) != null) {
                HashMap<String, String> map = new HashMap<>();
                String[] parts = line.split("\\|"); // name+value | type | labels
                String[] parts1 = parts[0].split(":");  // name | value
                String metricName = parts1[0];
                double value = Double.parseDouble(parts1[1]);
                String type = parts[1];
                String[] labels = parts[2].substring(1).split(","); // labels
                for (String label : labels) {
                    String[] entrys = label.split(":");
                    map.put(entrys[0], entrys[1]);
                }

                //System.out.println(n + " " + parts[0] + " " + parts[1] + " " + parts[2]);
                //System.out.println(n + " " + metricName + " " + value + " " + type);

                metrics.add(new Metric(metricName, value, type, map));
            }

        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return metrics;
    }


}
