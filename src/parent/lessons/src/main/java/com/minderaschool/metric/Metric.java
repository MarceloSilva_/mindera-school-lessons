package com.minderaschool.metric;

import java.util.HashMap;
import java.util.Map;

public class Metric {
    private String name;
    private double value;
    private String type;
    private Map<String,String> labels;

    public Metric(String name, double value, String type, HashMap<String,String> map){
        this.name = name;
        this.value=value;
        this.type = type;
        this.labels = map;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public String getType() {
        return type;
    }

    public Map<String, String> getLabels() {
        return labels;
    }
}
