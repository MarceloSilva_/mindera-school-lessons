package com.minderaschool.reverseArray;

import java.util.Scanner;

public class reverseArray {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] main) {
        int[] numbers = readNumbers();
        int[] invertedNumber = invertNumbers(numbers);
        printNumbers(invertedNumber);
    }

    public static int[] readNumbers() {
        //System.out.println("Quantity of numbers: ");
        int quantity = sc.nextInt();
        int[] numbers = new int[quantity];
        int a = 0;
        do {
            numbers[a] = sc.nextInt();
            //System.out.println(numbers[a]);
            a++;
        } while (a < quantity);

        return numbers;
    }

    public static int[] invertNumbers(int[] numbers) {
        int[] temp = new int[numbers.length];
        int x = 0;
        for (int i = numbers.length - 1; i >= 0; i--) {
            temp[x] = numbers[i];
            x++;
        }
        return temp;
    }

    public static void printNumbers(int[] numbers) {
        for (int num : numbers) {
            System.out.print(num + " ");
        }
    }
}
