package com.minderaschool.rock_papel_scissors;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Brain extends Thread {

    private int numberOfRounds;
    private boolean endGame = false;

    private final ScheduledThreadPoolExecutor threadPool = new ScheduledThreadPoolExecutor(4);

    private Player playerOne = new Player(1);
    private Player playerTwo = new Player(2);

    final static ConcurrentLinkedQueue<Plays> playerOneQueue = new ConcurrentLinkedQueue<>();
    final static ConcurrentLinkedQueue<Plays> playerTwoQueue = new ConcurrentLinkedQueue<>();

    private int playerOneWins;
    private int playerTwoWins;
    private int draws;

    public Brain() {
        this(5);

    }

    public Brain(int numberOfRounds) {
        this.numberOfRounds = numberOfRounds;

    }

    @Override
    public void run() {
        try {
            play();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void play() throws InterruptedException {

        int roundNum = 1;
        Semaphores.plays.release(2);

        while (!endGame) {
            if (checkPlays()) {

                Plays one = playerOneQueue.remove();
                Plays two = playerTwoQueue.remove();

                if (one == two) {
                    draws++;
                } else if (one == Plays.ROCK && two == Plays.SCISSORS) {   // p1  ROCK > SCISSORS
                    playerOneWins++;
                } else if (one == Plays.SCISSORS && two == Plays.ROCK) {    // p2  ROCK > SCISSORS
                    playerTwoWins++;
                } else if (one == Plays.SCISSORS && two == Plays.PAPER) {  // p1  SCISSORS > PAPER
                    playerOneWins++;
                } else if (one == Plays.PAPER && two == Plays.SCISSORS) {   // p2  SCISSORS > PAPER
                    playerTwoWins++;
                } else if (one == Plays.PAPER && two == Plays.ROCK) {       // p1  PAPER > ROCK
                    playerOneWins++;
                } else if (one == Plays.ROCK && two == Plays.PAPER) {       // p2  PAPER > ROCK
                    playerTwoWins++;
                }
                System.out.printf("PLAYER 1 : %s , PLAYER 2: %s\n", one, two);
                System.out.printf("PLAYER 1: %2d || DRAWS: %2d  || PLAYER 2: %2d\n\n", playerOneWins, draws, playerTwoWins);
                roundNum++;
                if (roundNum > numberOfRounds) {
                    endGame = true;
                    break;
                }
            }
            Semaphores.plays.acquire(2);
            threadPool.schedule(playerOne, 1, TimeUnit.SECONDS);
            threadPool.schedule(playerTwo, 1, TimeUnit.SECONDS);

        }


        if (playerOneWins == playerTwoWins) {
            System.out.println("DRAW");
        } else if (playerOneWins > playerTwoWins) {
            System.out.println("PLAYER 1 WINS");
        } else {
            System.out.println("PLAYER 2 WINS");
        }

    }

    private boolean checkPlays() {
        return !playerOneQueue.isEmpty() && !playerTwoQueue.isEmpty();
    }

}
