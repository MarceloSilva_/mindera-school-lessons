package com.minderaschool.rock_papel_scissors;

import java.util.Random;

public class Player implements Runnable {

    private int player;

    public Player(int playerNumber) {
        this.player = playerNumber;
    }

    @Override
    public void run() {
        Random random = new Random();
        int min = 0;
        int max = 2;
        int nr;
        Plays play;
        nr = random.nextInt(max - min);
        play = Plays.values()[nr];
        if (player == 1) {
            //System.out.println("PLAYER 1 : "+ play);
            Brain.playerOneQueue.add(play);
        } else {
            //System.out.println("PLAYER 2 : "+ play);
            Brain.playerTwoQueue.add(play);
        }
        Semaphores.plays.release(1);
    }
}
