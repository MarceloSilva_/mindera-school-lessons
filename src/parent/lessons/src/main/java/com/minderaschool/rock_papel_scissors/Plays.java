package com.minderaschool.rock_papel_scissors;

public enum Plays {
    ROCK,
    PAPER,
    SCISSORS
}
