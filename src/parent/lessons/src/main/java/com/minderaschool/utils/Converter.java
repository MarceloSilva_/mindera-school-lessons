package com.minderaschool.utils;


public class Converter {


    public static int getvalue(char num) {
        if (num == 'M') return 1000;
        if (num == 'D') return 500;
        if (num == 'C') return 100;
        if (num == 'L') return 50;
        if (num == 'X') return 10;
        if (num == 'V') return 5;
        if (num == 'I') return 1;
        return 0;
    }


    public static int romanToDecimal(String str) {
        int value = 0;

        for (int i = 0; i < str.length(); i++) {
            int val1 = getvalue(str.charAt(i));

            if (i + 1 < str.length()) {
                int val2 = getvalue(str.charAt(i + 1));

                if (val1 >= val2)
                    value += val1;
                else {
                    value += val2 - val1;
                    i++;
                }
            } else {
                value = value + val1;
                i++;
            }
        }

        return value;
    }
}