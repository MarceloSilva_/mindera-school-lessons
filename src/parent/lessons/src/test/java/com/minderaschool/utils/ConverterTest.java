package com.minderaschool.utils;

import static org.junit.jupiter.api.Assertions.*;

class ConverterTest {

    @org.junit.jupiter.api.Test
    void romanToDecimalShoulConvert1() {
        assertEquals(2,Converter.romanToDecimal("II"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalShouldConvert2() {
        assertEquals(4,Converter.romanToDecimal("IV"));
    }
}