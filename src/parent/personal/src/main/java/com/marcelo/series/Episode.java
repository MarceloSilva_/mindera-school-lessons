package com.marcelo.series;

public class Episode {

    private final String href;
    private final int season;
    private final int ep;

    public Episode(String href, int season,int ep){
        this.href = href;
        this.ep   = ep;
        this.season = season;
    }

    public int getSeason() {return season;}

    public String getHref() {
        return href;
    }

    public int getEp() {
        return ep;
    }

}
