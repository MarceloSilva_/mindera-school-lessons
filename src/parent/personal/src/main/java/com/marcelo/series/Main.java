package com.marcelo.series;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.lang.String;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


public class Main {

    private static String url = "http://tugaflix.xyz/serie?id=";
    public static List<Serie> series = new ArrayList<>();
    private static int n = 0;


    public static void main(String[] args) {
        read();
        printMenu();
    }

    // POUPULATE SERIES []-> getSeries() && []-> getWebEp()
    public static void read() {
        getSeries();
        for (int i = 0; i < series.size(); i++) {
            if (series.get(i) != null)
                getWebEp(i);
                //return;
        }
    }

    // GET LIST OF SERIES FROM FILE
    public static void getSeries() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("Series.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(" ");
                series.add(n,new Serie(parts[0], parts[1], Integer.parseInt(parts[2]), Integer.parseInt(parts[3])));
                //System.out.println(n + " " + parts[0] + " " + parts[1] + " " + parts[2] + " " + parts[3]);
                n++;
            }
        } catch (Exception e) {

        }
    }

    // GET LIST OF EPISODES OF A SERIE FROM THE WEBSITE
    public static void getWebEp(int index) {
        String urlSe = url + series.get(index).getUrlSerie();
        //System.out.println(series[index].getName());
        Episode eps;
        int num = 0;
        try {
            String html = Jsoup.connect(urlSe).get().html();
            Document doc = Jsoup.parse(html);
            Elements links = doc.select("figcaption span a");
            Iterator<Element> ite = links.iterator();
            //System.out.println(links.toString());
            while (ite.hasNext()) {
                Element link = ite.next();
                //System.out.println(link.toString());
                String linkHref = link.attr("href");
                String seasonEp = link.html();
                String[] parts = seasonEp.split("E");

                int season = Integer.parseInt(parts[0].replace("S", ""));
                int ep = Integer.parseInt(parts[1].replace("E", ""));
                eps= new Episode(linkHref, season, ep);

                //System.out.println("Link: " + eps[num].getHref() + " S" +eps[num].getSeason() + " E" +eps[num].getEp());
                num++;
                series.get(index).addEp(eps);
            }

        } catch (Exception e) {

        }
    }

    // ADD SERIE TO THE FILE []-> write()
    public static void addSerie() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Serie Name: ");
        String name = sc.next();
        System.out.print("URL: ");
        String url = sc.next();
        System.out.print("Season: ");
        int season = sc.nextInt();
        System.out.print("URL: ");
        int episode = sc.nextInt();
        write(name, url, season, episode, true);
    }

    // WRITE TO THE FILE
    public static void write(String name, String url, int season, int episode, boolean append) {
        String str = name + " " + url + ' ' + season + ' ' + episode;
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("Series.txt", true));
            writer.write(str);
            writer.newLine();
            writer.close();
        } catch (Exception e) {

        }
    }

    // PRINT A LIST OF A SERIE
    public static void printSerieEP() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Index: ");
        int i = sc.nextInt();
        System.out.printf("Serie: %s %s%s", series.get(i).getName(), url, series.get(i).getUrlSerie());
        List<Episode> eps = series.get(i).getEp();
        int season = 00;
        int num = 0;
        for (Episode ep : eps) {
            if (ep != null) {
                if (season != ep.getSeason()) {
                    num = 0;
                    season = ep.getSeason();
                    System.out.println("\n  Season " + season + ":");
                    System.out.print("    ");
                }
                if (season < series.get(i).getCurrentSeason()) {
                    System.out.printf("\u001B[32m%02d \u001B[0m", ep.getEp());
                } else if (season > series.get(i).getCurrentSeason()) {
                    System.out.printf("\u001B[31m%02d \u001B[0m", ep.getEp());
                } else {
                    if (ep.getEp() == series.get(i).getCurrentEp()) {
                        System.out.printf("\u001B[33m%02d \u001B[0m", ep.getEp());
                    } else if (ep.getEp() > series.get(i).getCurrentEp()) {
                        System.out.printf("\u001B[31m%02d \u001B[0m", ep.getEp());
                    } else {
                        System.out.printf("\u001B[32m%02d \u001B[0m", ep.getEp());
                    }
                }
                num++;
                if (num == 10) {
                    System.out.print("\n    ");
                    num = 0;
                }

            }
            //System.out.print(ep.getSeason() + "\n");
        }
        System.out.println();
    }

    // PRINT LIST OF SERIES WITH NEW EPISODES
    public static void printSeriesNew() {
        for (int i = 0; i < series.size(); i++) {
            if (series.get(i) != null)
                if (series.get(i).getCurrentSeason() < series.get(i).getLastSeason()) {
                    System.out.printf("\n%d - %s", i, series.get(i).getName());
                } else {
                    if (series.get(i).getCurrentEp() < series.get(i).getLastEpisodeFromSeason(series.get(i).getCurrentSeason())) {
                        System.out.printf("\n%d - %s", i, series.get(i).getName());
                    }
                }
        }
    }

    // PRINT A LIST OF ALL SERIES
    public static void printSeries() {
        for (int i = 0; i < series.size(); i++) {
            if (series.get(i) != null)
                System.out.printf("\n%d - %s", i, series.get(i).getName());
        }
    }

    // PRINT MENU
    public static void printMenu() {
        Scanner sc = new Scanner(System.in);
        int opt;
        do {
            System.out.println("\n----------------------");
            System.out.println("1 - List Series With New Episodes");
            System.out.println("2 - Mark Next Episode As Seen");
            System.out.println("3 - List Episodes Of A Serie");
            System.out.println("4 - List All Series");
            System.out.println("5 - Add New Serie");
            System.out.println("0 - Exit");
            System.out.println("----------------------");
            System.out.print("Select option: ");
            opt = sc.nextInt();
            switch (opt) {
                case 1:
                    printSeriesNew();
                    break;
                case 2:
                    addEp();
                    break;
                case 3:
                    printSerieEP();
                    break;
                case 4:
                    printSeries();
                    break;
                case 5:
                    addSerie();
                    break;
            }
        } while (opt != 0);

    }

    // SET NEXT EPISODE AS SEEN  []-> write()
    public static void addEp() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Index: ");
        int index = sc.nextInt();
        int currentEp = series.get(index).getCurrentEp();
        int currentSeason = series.get(index).getCurrentSeason();
        int lastEpSeason = series.get(index).getLastEpisodeFromSeason(currentSeason);
        int lastSeason = series.get(index).getLastSeason();
        if (currentEp + 1 > lastEpSeason) {
            if (currentSeason + 1 <= lastSeason) {
                series.get(index).setCurrentSeason(currentSeason + 1);
                series.get(index).setCurrentEp(01);
            }
        } else {
            series.get(index).setCurrentEp(currentEp + 1);
        }
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("Series.txt", false));
            writer.flush();
            writer.close();
            for (Serie se : series) {
                write(se.getName(), se.getUrlSerie(), se.getCurrentSeason(), se.getCurrentEp(), true);
            }
        } catch (Exception e) {

        }
    }

}