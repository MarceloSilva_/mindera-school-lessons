package com.marcelo.series;


import java.util.ArrayList;
import java.util.List;

public class Serie {
    private final String urlSerie;
    private final String name;
    private List<Episode> eps = new ArrayList<>();
    private int currentSeason;
    private int currentEp;

    public Serie(String name, String urlSerie, int currentSeason, int currentEp) {
        this.name = name;
        this.urlSerie = urlSerie;
        this.currentSeason = currentSeason;
        this.currentEp = currentEp;
    }

    public int getCurrentSeason() {
        return currentSeason;
    }

    public int getCurrentEp() {
        return currentEp;
    }

    public String getName() {
        return name;
    }

    public List<Episode> getEp() {
        return eps;
    }

    public String getUrlSerie() {
        return urlSerie;
    }

    public int getLastSeason() {
        int season = 0;
        for (int i = 0; i < eps.size(); i++) {
            if (eps.get(i).getSeason() > season) {
                season = eps.get(i).getSeason();
            }
        }
        return season;
    }

    public int getLastEpisodeFromSeason(int season) {
        int last = 0;
        for (int i = 0; i < eps.size(); i++) {
            if (eps.get(i).getSeason() == season) {
                if (eps.get(i).getEp() > last) {
                    last = eps.get(i).getEp();
                }
            }
        }
        return last;
    }

    public void setEps(List<Episode> eps) {
        this.eps = eps;
    }

    public void addEp(Episode eps) {
        this.eps.add(eps);
    }

    public void setCurrentSeason(int currentSeason) {
        this.currentSeason = currentSeason;
    }

    public void setCurrentEp(int currentEp) {
        this.currentEp = currentEp;
    }
}
